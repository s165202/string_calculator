package main;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringCalculator {

    public String Add(String numbers) throws Exception {
        // Allow spacing (optional)
        numbers = numbers.replace(" ", "");

        // Special case for empty string
        if(numbers.equals(""))
            return "0";

        // Define the delimiter
        String delimiter = ",";

        // If the string follows the regex of "//[delimiter]\n",
        //  the delimiter is changed as specified from the input
        Pattern regexPattern = Pattern.compile("//(.*?)//", Pattern.CASE_INSENSITIVE);
        Matcher match = regexPattern.matcher(numbers);
        if(match.find()) {
            // Getting the delimiter from the match
            delimiter = match.group(1);
            // Delete the redundant "//[delimiter]\n" from the input
            numbers = numbers.replace(match.group(0), "");
        }

        // Number splitting with delimiter and new lines
        numbers = numbers.replace("\n", delimiter);
        String[] stringArray = numbers.split(delimiter);

        // Addition
        int sum = 0;
        int actual;
        StringBuilder negativeNumbers = new StringBuilder("");
        for(int i=0; i<stringArray.length; i++){
            actual = Integer.parseInt(stringArray[i]);
            // Add negative numbers if they exist
            if(actual<0)
                negativeNumbers.append(" ").append(actual);
            // Otherwise, update the addition sum
            else
                sum += Integer.parseInt(stringArray[i]);
        }

        // If there are negative numbers, throw an exception
        if(!negativeNumbers.toString().equals(""))
            throw new Exception("negatives not allowed"+negativeNumbers);
        return String.valueOf(sum);
    }
}

package main;

public class CalculatorMain {

    public static void main(String[] args) {
        String argument = args[0];

        if(argument.equals("test build"))
            System.out.println("StringCalculator build success.");

        try {
            System.out.println(new StringCalculator().Add(argument));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

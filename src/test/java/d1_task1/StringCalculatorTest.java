package d1_task1;

import main.StringCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class StringCalculatorTest {

    StringCalculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new StringCalculator();
    }

    @Test
    void testAddPositiveNumbers() throws Exception {
        Assertions.assertEquals("0", calculator.Add(""));

        Assertions.assertEquals("1", calculator.Add("1"));

        Assertions.assertEquals("3", calculator.Add("1,2"));
        Assertions.assertEquals("3", calculator.Add("1, 2"));

        Assertions.assertEquals("10", calculator.Add("1, 2, 5, 2"));
    }

    @Test
    void testAddPositiveNumbersWithNewLines() throws Exception {
        Assertions.assertEquals("6", calculator.Add("1\n2,3"));
    }

    @Test
    void testAddPositiveNumbersWithDelimiters() throws Exception {
        Assertions.assertEquals("3", calculator.Add("//;\n1;2"));
        Assertions.assertEquals("3", calculator.Add("//;\n1\n2"));
        Assertions.assertEquals("24", calculator.Add("//_\n1\n2_10_10\n1"));
        Assertions.assertEquals("24", calculator.Add("//_;\n1\n2_;10_;10\n1"));
        Assertions.assertEquals("24", calculator.Add("//add\n1add2add10add10add1"));
    }

    @Test
    void testAddNegativeNumbers(){
        Assertions.assertEquals("negatives not allowed -10 -2", Assertions.assertThrows(Exception.class,
                () -> {calculator.Add("//_\n1\n2_-10_-2\n1");}).getMessage());

        Assertions.assertEquals("negatives not allowed -1 -2", Assertions.assertThrows(Exception.class,
                () -> {calculator.Add("-1,-2\n1");}).getMessage());

        Assertions.assertEquals("negatives not allowed -1 -2 -1", Assertions.assertThrows(Exception.class,
                () -> {calculator.Add("-0,-1\n-2, 8,9,-1,0,33");}).getMessage());
    }
}